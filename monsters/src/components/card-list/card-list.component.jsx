import Card from "../card/card.component";
import "./card-list.style.css";

const CardList = ({ items }) => (
  <div className="card-list">
    {items.map((item) => (
      <Card key={item.id} item={item} />
    ))}
  </div>
);

export default CardList;
