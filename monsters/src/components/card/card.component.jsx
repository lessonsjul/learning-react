import "./card.style.css";

const Card = ({ item }) => (
  <div className="card-container">
    <img
      alt={`monster ${item.name}`}
      src={`https://robohash.org/${item.id}?set=set2&size=180x180`}
    />
    <h2>{item.name}</h2>
    <p>{item.email}</p>
  </div>
);

export default Card;
