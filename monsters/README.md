# Monsters 

This project is aimed at learning the fundamentals of React.

This is a single-page application that displays cards of monsters, and it includes a search input that filters the monsters by name.

![Monsters preview](preview.png)