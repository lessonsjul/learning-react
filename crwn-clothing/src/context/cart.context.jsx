import {createContext, useReducer} from "react";

import {createAction} from "../utils/reducer/reducer.utils";

const addCartItem = (cartItems, productToAdd) => {
  const existingCartItem = cartItems.find(({id}) => id === productToAdd.id);

  if (existingCartItem) {
    return cartItems.map((item) =>
      item.id === productToAdd.id
        ? {...item, quantity: item.quantity + 1}
        : item
    );
  }

  return [...cartItems, {...productToAdd, quantity: 1}];
};

const clearCartItem = (cartItems, productToDelete) => {
  return [...cartItems.filter((item) => item.id !== productToDelete.id)];
};

const removeCartItem = (cartItems, productToRemove) => {
  const existingCartItem = cartItems.find(({id}) => id === productToRemove.id);

  if (existingCartItem.quantity > 1) {
    return cartItems.map((item) =>
      item.id === productToRemove.id
        ? {...item, quantity: item.quantity - 1}
        : item
    );
  }

  return clearCartItem(cartItems, productToRemove);
};

export const CartContext = createContext({
  isCartOpen: false,
  setIsCartOpen: () => {},
  cartItems: [],
  addItemToCart: () => {},
  removeItemFromCart: () => {},
  clearItemFromCart: () => {},
  cartCount: 0,
  cartTotal: 0,
});

export const CART_ACTION_TYPES = {
  SET_CART_IS_OPEN: 'SET_CART_IS_OPEN',
  SET_CART_ITEMS: 'SET_CART_ITEMS'
}

export const cartReducer = (state, action) => {
  const {type, payload} = action;

  switch (type) {
    case CART_ACTION_TYPES.SET_CART_IS_OPEN:
      return {
        ...state,
        isCartOpen: payload
      };
    case CART_ACTION_TYPES.SET_CART_ITEMS:
      return {
        ...state,
        ...payload
      }
    default:
      throw new Error(`Unhandled type ${type} in cartReducer`)
  }
};

const INITIAL_STATE = {
  isCartOpen: false,
  cartItems: [],
  cartCount: 0,
  cartTotal: 0
}

export const CartProvider = ({children}) => {
  const [{ isCartOpen, cartItems, cartCount, cartTotal }, dispatch] = useReducer(cartReducer, INITIAL_STATE);

  const updateCartItemsReducer = (newCartItems) => {
    const newCartCount = newCartItems.reduce((accumulator, item) => accumulator + item.quantity, 0);
    const newCartTotal = newCartItems.reduce((accumulator, item) => accumulator + item.quantity * item.price, 0);
    dispatch(
      createAction(
        CART_ACTION_TYPES.SET_CART_ITEMS,
        {
          cartItems: newCartItems,
          cartCount: newCartCount,
          cartTotal: newCartTotal
        }
      )
    );
  };

  const addItemToCart = (productToAdd) => {
    updateCartItemsReducer(addCartItem(cartItems, productToAdd))
  };

  const clearItemFromCart = (productToDelete) => {
    updateCartItemsReducer(clearCartItem(cartItems, productToDelete))
  };

  const removeItemFromCart = (productToDelete) => {
    updateCartItemsReducer(removeCartItem(cartItems, productToDelete))
  };

  const setIsCartOpen = (isCartOpen) => {
    dispatch(createAction(CART_ACTION_TYPES.SET_CART_IS_OPEN, isCartOpen));
  };

  const value = {
    isCartOpen,
    setIsCartOpen,
    addItemToCart,
    cartItems,
    cartCount,
    cartTotal,
    removeItemFromCart,
    clearItemFromCart,
  };

  return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
};
