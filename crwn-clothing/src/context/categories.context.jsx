import { createContext, useEffect, useState } from "react";

import { getCollectionAndDocuments } from "../utils/firebase/firebase.utils.js";

export const CategoriesContext = createContext({
    categoriesMap: {},
    setCategoriesMap: () => {}
});

export const CategoriesProvider = ({ children }) => {
  const [categoriesMap, setCategoriesMap] = useState({});
  
    useEffect(() => {
      getCollectionAndDocuments("categories")
        .then(setCategoriesMap)
    }, []);

    const value = { categoriesMap };
  return <CategoriesContext.Provider value={value}>{children}</CategoriesContext.Provider>;
};
