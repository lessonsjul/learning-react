import { useContext } from "react";
import { BUTTON_TYPE_CLASSES } from "../button/button.component";
import { CartContext } from "../../context/cart.context";

import {
  ProductCardContainer,
  CardImage,
  CardButton,
  CardFooter,
  FooterName,
  FooterPrice,
} from "./product-card.style";

const ProductCard = ({ product }) => {
  const { name, price, imageUrl } = product;

  const { addItemToCart } = useContext(CartContext);

  const addProductHandler = () => addItemToCart(product);

  return (
    <ProductCardContainer>
      <CardImage src={imageUrl} alt={`${name}`} />
      <CardFooter>
        <FooterName>{name}</FooterName>
        <FooterPrice>{price}</FooterPrice>
      </CardFooter>
      <CardButton
        buttonType={BUTTON_TYPE_CLASSES.inverted}
        onClick={addProductHandler}
      >
        Add to cart
      </CardButton>
    </ProductCardContainer>
  );
};

export default ProductCard;
