import { useState, useCallback } from "react";

import {
  signInWithGooglePopup,
  signInAuthUserWithEmailAndPassword,
} from "../../utils/firebase/firebase.utils";

import FormInput from "../form-input/form-input.component";
import Button, { BUTTON_TYPE_CLASSES } from "../button/button.component";

import { ButtonsContainer, SignInContainer } from "./sign-in-form.style";

const defaultFormFields = {
  email: "",
  password: "",
};

const SignInForm = () => {
  const [formFields, setFormFields] = useState(defaultFormFields);
  const { email, password } = formFields;

  const resetFormFields = () => {
    setFormFields(defaultFormFields);
  };

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();

      signInAuthUserWithEmailAndPassword(email, password)
        .then(() => resetFormFields())
        .catch((error) => {
          if (error.code === "auth/wrong-password") {
            alert("Wrong password");
          } else if (error.code === "auth/user-not-found") {
            alert("User does not exists");
          } else {
            console.error("User creation encountered an error", error);
          }
        });
    },
    [email, password]
  );

  const inputChangeHandler = (event) => {
    const { name, value } = event.target;

    setFormFields({ ...formFields, [name]: value });
  };

  return (
    <SignInContainer>
      <h2>I already have an account</h2>
      <span>Sign in with your email and password</span>
      <form onSubmit={handleSubmit}>
        <FormInput
          label="Email"
          type="text"
          required
          onChange={inputChangeHandler}
          value={email}
          name="email"
        />
        <FormInput
          label="password"
          type="password"
          required
          onChange={inputChangeHandler}
          value={password}
          name="password"
        />
        <ButtonsContainer>
          <Button type="submit">Sign In</Button>
          <Button
            type="button"
            buttonType={BUTTON_TYPE_CLASSES.google}
            onClick={signInWithGooglePopup}
          >
            Google Sign In
          </Button>
        </ButtonsContainer>
      </form>
    </SignInContainer>
  );
};

export default SignInForm;
