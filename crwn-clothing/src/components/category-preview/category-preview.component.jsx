import ProductCard from "../product-card/product-card.component";
import { Title, Preview, CategoryPreviewContainer } from "./category-preview.style";

const CategoryPreview = ({ title, products }) => {
  return (
    <CategoryPreviewContainer>
      <Title to={`/shop/${title.toLowerCase()}`}>
        {title.toUpperCase()}
      </Title>
      <Preview>
        {products
          .filter((_, idx) => idx < 4)
          .map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
      </Preview>
    </CategoryPreviewContainer>
  );
};

export default CategoryPreview;
