import SignInForm from "../../components/sign-in-form/sign-in-form.component";
import SignUpForm from "../../components/sign-up-form/sign-up-form.component";

import { AuthenticationConatiner } from "./authentication.style";

const Authentication = () => {
  return (
    <AuthenticationConatiner>
      <SignInForm />
      <SignUpForm />
    </AuthenticationConatiner>
  );
};

export default Authentication;
