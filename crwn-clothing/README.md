# CRWN-CLOTHING

This clothing store website was built using React as a part of a React learning course. 

The application is connected to Firebase for storing data related to categories and products, as well as for authentication purposes.

The application has been deployed on Netlify https://symphonious-llama-228ba8.netlify.app.

### Run locally
```ssh
npm start
```

## Installed libraries
* sass
* react-router-dom@6
* firebase
* styled-components
  